#Mail Mockup

This project was bootstrapped with [Create React App].

## About Author

I am selva. I am an angular developer. This is my first assignment in React

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

It also runs the **node server** in http://localhost:4510 port to serve the data.

I am using **concurrently** node package to achieve the same


### `npm test`

Launches the test runner in the interactive watch mode.

## Extra Packages Used in this Application

* react-bootstrap
  - This package is used for the purpose of bootstrap styling

* react-icons
  - This package is used with the intention to use the icons which are used in the package

* react-moment
  - This package is used for the purpose of date formatting

## Exclusions in this assignment
* Unread / Read mail management
* Stylings are not pixel perfect. Unable to find the exact pixel level details from pdf.
* Unable to find the exact icons in fontawesome, Hence most related icons are used.
* Few sample test cases were written
