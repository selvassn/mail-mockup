import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Moment from 'react-moment';
import { faStar, faReply, faShare } from '@fortawesome/free-solid-svg-icons';

export default class MailContent extends Component {
    
    render() {
        return (
            <div className="col-8 mail-content">
                <div className='mail-header row'>
                    <h4 className="col-10"> {this.props.mailData.subject} </h4>
                    <div className='icon-container col-2 text-right'>
                        <span className='small-font'> <FontAwesomeIcon icon={faReply} /></span>
                        <span className='small-font'> <FontAwesomeIcon icon={faShare} /></span>
                    </div>
                </div>
                <div className='mail-content-container col-12'>
                    <div className='mail-content-header'>
                        <div className='row'>
                            <div className='col-9'><span className='small-font'>From:</span> <span>{this.props.mailData['sender name']}</span></div>
                            <div className='col-3 small-font text-right date-container'>
                                <span><Moment format="MMM d">
                                    {this.props.mailData.date * 1000}
                                </Moment></span>
                                <FontAwesomeIcon icon={faStar} />
                            </div>
                        </div>
                        <div><span className='small-font'>To:</span> <span> {this.props.mailData['sender name']} </span> </div>
                    </div>
                    <div className="mail-content-data">
                        <span dangerouslySetInnerHTML={
                            { __html: this.props.mailData.content }
                        }></span>
                    </div>
                </div>
            </div>
        )
    }
}
