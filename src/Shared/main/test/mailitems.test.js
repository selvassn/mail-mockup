import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import MailItems from '../mailitems';

let mockData = {};
beforeAll(() => {
    mockData = require('../test/mockData/data.json'); 
    mockData = mockData.accounts[0];
});

test('Check if items exists', () => {
    const { container } = render(<MailItems accountData={mockData} />);
    const list = container.getElementsByTagName('li');
    expect(list).not.toBeNull();
});