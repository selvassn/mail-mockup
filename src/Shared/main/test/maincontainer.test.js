import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import MainContainer from '../maincontainer';
import data from '../test/mockData/data.json';

test('Expect the sidebar in the app', () => {
    const { container } = render(<MainContainer />)
    expect(container.getElementsByTagName('MailHeader')).toBeInTheDocument;
});

test('Expect the sidebar in the app', () => {
    const { container } = render(<MainContainer />)
    expect(container.getElementsByTagName('MailContent')).toBeInTheDocument;
});

test('Expect the sidebar in the app', () => {
    const { container } = render(<MainContainer />)
    expect(container.getElementsByTagName('MailItems')).toBeInTheDocument;
});

test('Mock the fetch call', async () => {
    const mockData = require('../test/mockData/data.json'); 
    const fakeFetch = {
        get: jest.fn(() => Promise.resolve({ data: mockData }))
    };
    const data = await fakeFetch.get('/api/getData');
    expect(data).toBeDefined();
})