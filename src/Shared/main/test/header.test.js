import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import MailHeader from '../header';

let mockData = {};
beforeAll(() => {
    mockData = require('../test/mockData/data.json'); 
});

test('Check if search box exists', () => {
  const { getByPlaceholderText } = render(<MailHeader accountDetails={mockData} />);  
  const ele = getByPlaceholderText(/search/i);
  expect(ele).toBeInTheDocument();
});

test('Check if account renders properly', () => {
    const { getByText } = render(<MailHeader accountDetails={mockData} />); 
    const linkElement = getByText(/Gustav/i);
    expect(linkElement).toBeInTheDocument();
  });
