import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import MailContent from '../mailcontent';

let mockData = {};
beforeAll(() => {
    mockData = require('../test/mockData/data.json'); 
    mockData = mockData.accounts[0].mail[0];
});

test('Check if header is present', () => {
    const { container } = render(<MailContent mailData={mockData} />);
    const list = container.getElementsByTagName('h4');
    expect(list).not.toBeNull();
});