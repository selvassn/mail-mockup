import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Moment from 'react-moment';
import { faStar } from '@fortawesome/free-solid-svg-icons';

export default class MailItems extends Component {
    constructor(props) {
        super(props);
        this.sortDataByDate();
    }
    /**
     * sortDataByDate
     * Sort the mails based on date
     */
    sortDataByDate() {
        this.props.accountData.mail.sort((a, b) => (a.date < b.date) ? 1 : -1);
    }

    /**
     * loadMailData
     * @param {number} index 
     */
    loadMailData(index) {
        this.props.onSelectMail(this.props.accountData.mail[index]);
    }

    render() {
        return (
            <div className="col-4 mail-items">
                <ul>
                    {this.props.accountData.mail.map((value, index) => {
                        return <li className="row" key={index} onClick={() => this.loadMailData(index)}>
                            <div className="col-md-1">
                                <input type="checkbox"></input>
                            </div>
                            <div className='col-md-8'>
                                <p> {value.subject} </p>
                                <p className="sender-name"> {value['sender name']}</p>
                            </div>
                            <div className="col-md-3 small-font text-right date-container">
                                <span>
                                    <Moment format="MMM d">
                                        {value.date * 1000}
                                    </Moment></span>
                                <FontAwesomeIcon icon={faStar} />
                            </div>
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}
