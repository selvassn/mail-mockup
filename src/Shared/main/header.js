import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Popover, OverlayTrigger, Button } from 'react-bootstrap';
import { faTrashAlt, faLocationArrow, faStopCircle } from '@fortawesome/free-solid-svg-icons';
export default class MailHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedAccIndex: 0 };
    }
    /**
     * update the state to selected index
     * Close the popover while switch the account
     * @param {number} index 
     */
    switchAccount(index) {
        this.setState({ selectedAccIndex: index });
        this.props.onSelectAccount(index);
        this.refs.overlay.hide();
    }

    render() {
        const popover = (
            <Popover id="popover-basic">
                <Popover.Title as="h3">Accounts</Popover.Title>
                <Popover.Content>
                    <ul>
                        {this.props.accountDetails.accounts.map((value, index) => {
                            return <li key={index}><a href='javascript:void(0);' onClick={() => this.switchAccount(index)} > {value.name} {value.surname}</a></li>
                        })}
                    </ul>
                </Popover.Content>
            </Popover>
        );

        return (
            <div>
                <div className="header row">
                    <div className="col-md-8">
                        <input
                            type="text"
                            className="input"
                            id="addInput"
                            placeholder="Search"
                        />
                    </div>
                    <div className="col-md-4 text-right">
                        <OverlayTrigger ref="overlay" placement="bottom" trigger="click" overlay={popover}>
                            <Button variant="link">{this.props.accountDetails.accounts[this.state.selectedAccIndex].name} {this.props.accountDetails.accounts[this.state.selectedAccIndex].surname}</Button>
                        </OverlayTrigger>
                    </div>
                </div>
                <div className="sub-header row">
                    <div className='col'>
                        <input type="checkbox" />
                        <div className='icon-container'>
                            <span className='small-font'> <FontAwesomeIcon icon={faLocationArrow} /></span>
                            <span className='small-font'> <FontAwesomeIcon icon={faTrashAlt} /></span>
                            <span className='small-font'> <FontAwesomeIcon icon={faStopCircle} /></span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
