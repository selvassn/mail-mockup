import React, { Component } from 'react';
import './maincontainer.scss';
import MailHeader from './header';
import MailItems from './mailitems';
import MailContent from './mailcontent';

export default class MainContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            account: {}, selectedAccIndex: 0, selectedAccount: {}, selectedMailData: {}
        }
    }

    /**
     * componentWillMount
     * using this lifecycle method to fetch the data from server before render happens
     */
    componentWillMount() {
        fetch('/api/getData')
            .then((res) => res.json())
            .then((data) => {
                console.log('data:', data);
                this.setState({ account: data, selectedAccIndex: 0, selectedAccount: data.accounts[0], selectedMailData: data.accounts[0].mail[0] });
            }).catch((e) => {
                console.log(e);
            })
    }

    /**
     * getAcccountData
     * Callback from MailHeader component 
     * Update the state to the current user account while switch the account
     */
    getAcccountData = (accountIndex) => {
        this.setState({ selectedAccount: this.state.account.accounts[accountIndex] });
        this.setState({ selectedMailData: this.state.account.accounts[accountIndex].mail[0] });
    }

    /**
     * selectedMailDetails
     * Callback from MailItems component 
     * Update the state of selected mail item
     */
    selectedMailDetails = (data) => {
        this.setState({ selectedMailData: data });
    }

    render() {
        if (this.state.account && this.state.account.accounts && this.state.account.accounts.length) {
            return (
                <div className="container main-mail-container">
                    <MailHeader accountDetails={this.state.account} onSelectAccount={this.getAcccountData} />
                    <div className="row content-row">
                        <MailItems accountData={this.state.selectedAccount} onSelectMail={this.selectedMailDetails} />
                        <MailContent mailData={this.state.selectedMailData}></MailContent>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}
