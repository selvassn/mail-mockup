import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from 'react-bootstrap/Button';
import { faInbox, faEdit, faLocationArrow, faStopCircle, faTrashAlt, faMailBulk } from '@fortawesome/free-solid-svg-icons'
import './sidebar.scss';

export default class Sidebar extends Component {
    /**
     * To render the sidebar hard coded menu items
     */
    render() {
        const elements = [{
            name: 'Inbox',
            icon: faInbox
        }, {
            name: 'Drafts',
            icon: faEdit
        }, {
            name: 'Sent',
            icon: faLocationArrow
        }, {
            name: 'Span',
            icon: faStopCircle
        }, {
            name: 'Trash',
            icon: faTrashAlt
        },];

        return (

            <div className='sidebar'>
                <h4><FontAwesomeIcon icon={faMailBulk} />Mail</h4>
                <div className="text-center">
                    <Button variant="primary">Compose</Button>
                </div>
                <ul className="flex-column">
                    {elements.map((value, index) => {
                        return <li className="fa FaInbox" key={index}>  <FontAwesomeIcon icon={value.icon} /> {value.name}</li>
                    })}
                </ul>
            </div>
        )
    }
}
