import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import Sidebar from './sidebar';

test('renders learn react link', () => {
  const { getByText } = render(<Sidebar />);
  const linkElement = getByText(/Inbox/i);
  expect(linkElement).toBeInTheDocument();
});
