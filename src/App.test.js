import React from 'react';
import { render, getByPlaceholderText } from '@testing-library/react';
import App from './App';

test('Expect the sidebar in the app', () => {
  const { container } = render(<App />)
  expect(container.getElementsByTagName('Sidebar')).toBeInTheDocument;
});

test('Expect the sidebar in the app', () => {
  const { container } = render(<App />)
  expect(container.getElementsByTagName('MainContainer')).toBeInTheDocument;
});
