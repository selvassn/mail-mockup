import React from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import Sidebar from './Shared/sidebar/sidebar';
import MainContainer from './Shared/main/maincontainer';

function App() {
  return (
    <div className="container-fluid main-container">
      <div className="flex-xl-nowrap row main-row">
        <div className="sidebar col-md-2">
          <Sidebar />
        </div>
        <div className="col-md-10 pl-0">
          <MainContainer></MainContainer>
        </div>
      </div>
    </div>
  );
}

export default App;
