const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 4510;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/getData', (req, res) => {
    res.send({
        "accounts": [
            {
                "address": "g.fugger@mail.com",
                "mail": [
                    {
                        "content": "Dear Sir, <div> <p>Please send your business proposal asap</p> <p>Thanks</p> </div>",
                        "date": 1575474762.689671,
                        "sender email": "r.volkwist@hotmail.com",
                        "sender name": "Robert ",
                        "subject": "Business Proposal"
                    },
                    {
                        "content": "Dear Madam, <p> your tenant is very noise. </p>Their music keeps me up at night!",
                        "date": 1575473762.259641,
                        "sender email": "a.jacobs@msd.com",
                        "sender name": "Aletta Jacobs",
                        "subject": "Noise from upstairs"
                    }
                ],
                "name": "Gustav",
                "surname": "Fugger"
            },
            {
                "address": "h.g.d.lorin@mail.com",
                "mail": [
                    {
                        "content": "Dear Madam, \n I am writing to you to propose ><script> alert('You may be compromised') </script>",
                        "date": 1575474762.689671,
                        "sender email": "r.volkwist@hotmail.com",
                        "sender name": "Robert Volkwist",
                        "subject": "Water Charges"
                    },
                    {
                        "content": "Dear Madam, \n your tenant is <b>very</b> noise. Their music keeps me up at night!",
                        "date": 1575473762.259641,
                        "sender email": "a.jacobs@msd.com",
                        "sender name": "Aletta Jacobs",
                        "subject": "Update From building"
                    }
                ],
                "name": "Henrietta",
                "surname": "Van Groningen Der Lorin"
            }
        ]
    });
});

app.listen(port, () => console.log(`Node server is istening on port ${port}`));